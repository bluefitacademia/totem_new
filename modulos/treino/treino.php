<?php

include('../../classes/conexao.php');

$rotina = $_GET['rotina'];
$cod = $_GET['cod'];

$query = "select max(id_treino) as id_treino from bd_cgtraining.dbo.tb_treino_aluno where cod_aluno = '$cod'";

$bd = odbc_exec($conn , $query) or die($query);

while (odbc_fetch_row($bd)) {
  $id_treino = odbc_result($bd, "id_treino");
}

$query = "select exe.EXERCICIOS, m.SERIE, m.REPETICAO,  m.carga, ta.PROFESSOR, p.nome, ta.TREINO, r.CONTADOR, r.REPETICAO as repet, m.TIPO, m.obs  from BD_CGTRAINING.dbo.tb_exe_aluno_musc m
          left join BD_CGTRAINING.dbo.TB_ROTINA r on r.ID_ROTINA=m.ID_ROTINA
          left join BD_CGTRAINING.dbo.TB_EXERCICIOS exe on exe.ID_EXERCICIOS = m.ID_EXERCICIO
		      left join BD_CGTRAINING.dbo.TB_TREINO_ALUNO ta on ta.ID_TREINO=m.ID_TREINO
		      left join bd_cg.dbo.TB_PESSOAS p on p.cod_aluno = m.cod_aluno
          where r.cod_aluno = '$cod' and rotina = '$rotina' and ta.id_treino = $id_treino
          order by ordem*1";

$bd = odbc_exec($conn , $query) or die($query);

while (odbc_fetch_row($bd)) {

    $nome = odbc_result($bd, "nome");
    $prof = odbc_result($bd, "PROFESSOR");
    $treino = odbc_result($bd, "TREINO");
    $realizados = odbc_result($bd, "CONTADOR");
    $repeticao = odbc_result($bd, "repet");
    }

  ?>


<script>

function imprimir(){

    window.print();
    r = confirm("Deseja marca presença nesse treino?")

    if(r == true){
        window.location.assign('modulos/presenca/confirma.php?cod_aluno=<?php echo $cod; ?>&rotina=<?php echo $rotina; ?>');
    } else {
    window.location.assign('index.php');
  }

}

</script>


<center><h3 class='noprint'>Impressão de Treino (Rotina <?php echo $rotina; ?>) &nbsp; &nbsp;<a href='#' class='btn btn-primary btn-lg' onclick="imprimir()"><span class='glyphicon glyphicon-print'></span></a></h3></center>


<div class='fundo-treino_p'>
<center><img src='logo_colorido.png' class='logo-treino'>
  <hr  class='print'>

</center>
<b>Nome:</b> <?php echo $nome; ?><br>
<b>Cod. Aluno:</b> <?php echo $cod; ?><br>
<b>Treino:</b> <?php echo $treino; ?><br>
<b>Professor:</b> <?php echo $prof; ?><br>
<b>Realizado:</b> <?php echo $realizados; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<b>Restante:</b> <?php echo $repeticao - $realizados ?>&nbsp;&nbsp;|&nbsp;&nbsp;<b>Rotina:</b> <?php echo $rotina; ?>

<hr  class='print'>
<center><h4>Exercícios</h4></center>


<?php


$bd = odbc_exec($conn , $query) or die($query);

while (odbc_fetch_row($bd)) {


  ?>


    <table style='border-bottom: solid 1px #000; width: 90%' border="0">
      <tr>
        <td colspan="2"><?php  echo utf8_encode(odbc_result($bd, "EXERCICIOS"));?></td>
      </tr>
      <tr>
        <td><b>Séries:</b> <?php  echo odbc_result($bd, "SERIE");?>
        <td><b>Repetições:</b>  <?php  echo odbc_result($bd, "REPETICAO");?>
      </tr>
      <tr>
        <td><b>Carga:</b>  <?php  echo odbc_result($bd, "CARGA").' '.odbc_result($bd, "TIPO");?>
        <td>
      </tr>
      <tr>
        <td colspan="2"><b>Obs.:</b><br> <?php  echo odbc_result($bd, "obs") ?><br>

      </tr>
    </table>

    <?php
        }
    ?>
<br><br>
    Observações Importantes:<br>
    <b>-> Evite jejum prolongado<br>
       -> Mantenha se sempre hidratado<br>
       -> Qualquer dúvida procure o professor<br>


</div>
