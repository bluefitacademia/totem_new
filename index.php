<?php

session_start();

session_destroy();

include('classes/funcoes.php');


$erro = isset($_REQUEST['erro'])?1:0;


if($erro == 1){?>

<script>
    alert('Por favor efetue seu login!');
</script>

<?php } ?>

<html lang="en">
<head>
  <title>Totem BlueFit</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/index.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="plugins/jquery.maskedinput.pack.js"/></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" href="ico.png" />
</head>
<body>



<script>

function mostrateclado(){
  $('.teclado').show(); // aparece o div
  $('.tecladoano').hide(); // aparece o div

};

function mostratecladoano(){
  $('.teclado').hide(); // aparece o div
  $('.tecladoano').show(); // aparece o div
};

function teclado(texto){
      if(texto == 'C'){
          login.cpf.value = '';
      } else {
        if (texto == 'ok') {
          login.ano.focus();
      } else {
          login.cpf.value += texto;
      }
  }
}

function tecladoano(texto){
      if(texto == 'C'){
          login.ano.value = '';
      } else {
        if (texto == 'ok') {
          login.submit();
      } else {
          login.ano.value += texto;
      }
  }
}



</script>


<form name="login"  action='login.php' autocomplete="off">



<center>



<div class='fundo'>

</div>



<div class='login'>
  <img src="logo_colorido.png" class='logo'>
<h3>CPF / Cod. Aluno</h3>
  <input type="text" name="cpf" class='form-control' style='width: 70%; font-size: 35px; height: 45px; text-align: center' onfocus="mostrateclado()">

<h3>Ano Nascimento</h3>
  <input type="text" name="ano" class=form-control style='width: 70%; font-size: 35px; height: 45px; text-align: center'  onfocus="mostratecladoano()">

<br><br>

  <a href='#' class='btn btn-primary' onclick="login.submit();" style=' height: 60px; width:60px; font-size: 35px; padding-top:10px;'><span class='glyphicon glyphicon-ok'></span></a>
  &nbsp;&nbsp;&nbsp;
  <a href='index.php' class='btn btn-danger' style=' height: 60px; width:60px; font-size: 35px; padding-top:10px;'><span class='glyphicon glyphicon-remove'></span></a>

</form>

<div class='teclado'>
<center>
  <br>
    <div class='linha'>
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('1')">1</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('2')">2</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('3')">3</a>
    </div>
    <br>
    <div class='linha'>
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('4')">4</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('5')">5</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('6')">6</a>
    </div>
    <br>
    <div class='linha'>
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('7')">7</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('8')">8</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('9')">9</a>
    </div>
    <br>
    <div class='linha'>
      <a href='#' class='btn btn-danger btn-lg' style='width:60px; height:60px; font-size:30px;'  onclick="teclado('C')">C</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('0')">0</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-success btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="teclado('ok')">ok</a>
    </div>
  <br>
</div>


<div class='tecladoano'>
<center>
  <br>
    <div class='linha'>
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('1')">1</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('2')">2</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('3')">3</a>
    </div>
    <br>
    <div class='linha'>
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('4')">4</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('5')">5</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('6')">6</a>
    </div>
    <br>
    <div class='linha'>
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('7')">7</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('8')">8</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('9')">9</a>
    </div>
    <br>
    <div class='linha'>
      <a href='#' class='btn btn-danger btn-lg' style='width:60px; height:60px; font-size:30px;'  onclick="tecladoano('C')">C</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-primary btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('0')">0</a>&nbsp;&nbsp;&nbsp;
      <a href='#' class='btn btn-success btn-lg' style='width:60px; height:60px; font-size:30px;' onclick="tecladoano('ok')">ok</a>
    </div>
  <br>
</div>


</div>
