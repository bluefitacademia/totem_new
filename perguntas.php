<?php
//Abro a sessão para pegar o codigo do aluno logado, ja aproveito e verifico se existe alguma tentativa de acesso sem login
session_start();
        $cod_aluno = isset($_SESSION['cod'])?$_SESSION['cod']:'deslogar';

        if($cod_aluno == "deslogar"){?>
              <script>
                  window.location.assign('index.php?erro=1');
              </script>
  <?php }

//incluo a conexão com o banco de dados
include('classes/conexao.php');

//crio as váriaveis de resposta
$r_cod = isset($_GET['cod'])?$_GET['cod']:'';
$r_aluno = isset($_GET['aluno'])?$_GET['aluno']:'';
$r_resp = isset($_GET['resposta'])?$_GET['resposta']:'';


//caso exista uma resposta faço o insert na tabela de respostas e encaminho para a tela principal
if($r_resp != ''){

$query = "insert into bd_totem.dbo.tb_respostas_aluno (cod_pergunta, cod_aluno, resposta, data) values ($r_cod, $r_aluno, '".utf8_decode($r_resp)."', getdate())";

$bd = odbc_exec($conn , $query);?>


<script>
  window.location.assign('principal.php');
</script>


<?php


} else {
 ?>





<?php
// se não tiver resposta verifico se existe alguma pergunta ativa
$query = "select * from bd_totem.dbo.tb_perguntas where data_fim >=getdate() and excluido=0";

$bd = odbc_exec($conn , $query);

$conta = 0;

while (odbc_fetch_row($bd)) {
           $cod_pergunta = odbc_result($bd, "cod_pergunta");
           $pergunta = odbc_result($bd, "pergunta");
           $conta ++;
      }

$hoje = date('m/d/Y');


// verifico se o aluno logado ja respondeu a pergunta no dia se sim encaminho para área principal
$query = "select * from bd_totem.dbo.tb_respostas_aluno where cod_pergunta=$cod_pergunta and cod_aluno = $cod_aluno and data = '$hoje'";

$bd = odbc_exec($conn , $query);
$cancela = 0;
while (odbc_fetch_row($bd)) {
      $cancela = 1;
      }

if($cancela == 1){?>

  <script>
    window.location.assign('principal.php');
  </script>

  <?php

} else{


//se tiver alguma pergunta ativa apresento a tela com a pergunta
if($conta > 0){

?>



<html lang="en">
<head>
  <title>Totem BlueFit</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/principal.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="plugins/jquery.maskedinput.pack.js"/></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" href="ico.png" />
</head>
  <body>

<center>
  <img src='logo_colorido.png' height=80px; style='margin: 50px;'>
</center>

    <div class="panel panel-primary" style='width: 80%; margin-left: 10%;'>
      <div class="panel-heading">Pergunta</div>
      <div class="panel-body">
          <center><h1><?php echo utf8_encode($pergunta); ?></h1>

            <!-- nesse modelo só vou conseguir salvar sim não, se existir algum outro modelo precisarei mudar o código -->
              <a href="perguntas.php?cod=<?php echo $cod_pergunta; ?>&aluno=<?php echo $cod_aluno; ?>&resposta=sim" class='btn btn-success btn-lg'>SIM</a> &nbsp;&nbsp;&nbsp;
              <a href="perguntas.php?cod=<?php echo $cod_pergunta; ?>&aluno=<?php echo $cod_aluno; ?>&resposta=não" class='btn btn-danger btn-lg'>Não</a> &nbsp;&nbsp;&nbsp;


          </center>


      </div>
    </div>



  </body>
</html>
<?php }
else {?>

<script>
  window.location.assign('principal.php');
</script>


  <?php }
}
}?>
